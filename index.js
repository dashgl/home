"use strict";


/**
 * IMPORTS
 **/

const fs = require('fs');
const path = require('path');
const http = require('http');
const mime = require('mime');
const sprintf = require('sprintf');
const Handlebars = require('handlebars');
const serveStatic = require('serve-static');
const finalhandler = require('finalhandler');

/**
 * SET UP SERVER
 **/

const toc = {
	info : [
		{
			file: "info/00_about.html",
			slug : "about",
			label : "What is DashGL"
		},
		{
			file: "info/01_mascot.html",
			slug : "mascot",
			label : "Mascot Dashie"
		},
		{
			file: "info/02_resources.html",
			slug : "resources",
			label : "OpenGL Resources"
		},
		{
			file: "info/03_contribute.html",
			slug : "contribute",
			label : "Contribute"
		}
	],
	basics : [
		{
			file: "basics/00_intro.html",
			slug : "forward",
			label : "Forward"
		},
		{
			file: "basics/01_triangle.html",
			slug : "triangle",
			label : "Blue Triangle"
		},
		{
			file: "basics/02_square.html",
			slug : "square",
			label : "Red Square"
		},
		{
			file: "basics/03_circle.html",
			slug : "circle",
			label : "Green Circle"
		},
		{
			file: "basics/04_vcolor.html",
			slug : "color",
			label : "Fragment Color"
		},
		{
			file: "basics/05_multiple.html",
			slug : "multiple",
			label : "Multiple Shapes"
		},
		{
			file: "basics/06_fade.html",
			slug : "fade",
			label : "Fade In and Out"
		},
		{
			file: "basics/07_trans.html",
			slug : "transform",
			label : "Transform Triangle"
		},
		{
			file: "basics/08_cube.html",
			slug : "cube",
			label : "Draw a Cube"
		},
		{
			file: "basics/09_tex.html",
			slug : "texture",
			label : "Textured Cube"
		},
		{
			file: "basics/10_dashie.html",
			slug : "dashie",
			label : "Render Model"
		}
	],
	brickout : [
		{
			file: "brickout/00_intro.html",
			slug : "forward",
			label : "Forward"
		}
	],
	invaders : [
		{
			file: "invaders/00_intro.html",
			slug : "forward",
			label : "Forward"
		}
	],
	astroids : [
		{
			file: "astroids/00_intro.html",
			slug : "forward",
			label : "Forward"
		}
	]
}

const serve = serveStatic('public');
const src = fs.readFileSync('template.html').toString();
const template = Handlebars.compile(src);

/**
 * HTTP SERVER
 **/

const server = http.createServer(async function(req, res) {
	
	switch((path.extname(req.url))) {
	case '.html':
	case '':
		let loc = resolveLocation(req.url);
		res.end(template(
			{
				'desktop-menu' : renderDekstopMenu(loc),
				'article' : await renderArticle(loc)
			}
		));
		break;
	default:
		serve(req, res, finalhandler(req, res));
		break;
	}


}).listen(8090, function() {
	
	console.log("Sevrer is listening on port 8090");

});

/**
 * Resolve Location
 **/

function resolveLocation(url) {
	
	let article, folder;
	let parts = url.split("/");
	parts.shift();
	parts.pop();

	folder = parts[0];
	if(!toc[folder]) {
		folder = "basics";
	}

	article = parts[1];
	const list = toc[folder];
	let found = false;
	for(let i = 0; i < list.length; i++) {
		if(article !== list[i].slug) {
			continue;
		}
		found = true;
		break;
	}

	if(!found) {
		article = list[0].slug;
	}

	return {
		folder : folder,
		article : article
	};

}

/**
 * Render Desktop Menu
 **/

function renderDekstopMenu(loc) {

	let str = "";
	const tmp_a = `
		<a href='%s'>
			<li>
				%s
			</li>
		</a>
	`;

	const tmp_b = `
		<a href='%s'>
			<li class='active'>
				%s
			</li>
		</a>
	`;
	
	let list = toc[loc.folder];
	for(let i = 0; i < list.length; i++) {
		
		let cl = tmp_a;
		if (loc.article === list[i].slug) {
			cl = tmp_b;
		}

		let href = "/" + loc.folder + "/" + list[i].slug + "/";
		str += sprintf(cl, href, list[i].label);

	}
	
	return str;

}

/**
 * Render Article
 **/

function renderArticle(loc) {
	
	return new Promise (function (resolve, reject) {

		let list = toc[loc.folder];
		let filename;
		for(let i = 0; i < list.length; i++) {
			if (loc.article !== list[i].slug) {
				continue;
			}
			filename = list[i].file;
			break;
		}

		fs.readFile(filename, function(err, data) {
			if(err) {
				throw err;
			}

			resolve(data.toString());
		});

	});

}

/**
 * End
 **/
