"use strict";

const Table = {

	"hello" : {
		"00_intro" : {
			title : "Hello Overview",
			author : "Kion",
			created_on : "Jan 20, 2020",
			last_edit : "March 06, 2020"
		},
		"01_tri" : {
			title : "Blue Triangle",
			author : "Kion",
			created_on : "March 06, 2020",
			last_edit : "March 06, 2020"
		}
	},

	"brickout" : {
		"01_intro" : {
			title : "Brickout Overview",
			author : "Kion",
			created_on : "Jan 20, 2020",
			last_edit : "Jan 20, 2020"
		}

	},

	"invaders" : {
		"01_intro" : {
			title : "Invaders Overview",
			author : "Kion",
			created_on : "Jan 20, 2020",
			last_edit : "Jan 20, 2020"
		}
	},

	"astroids" : {
		"01_intro" : {
			title : "Astroids Overview",
			author : "Kion",
			created_on : "Jan 20, 2020",
			last_edit : "Jan 20, 2020"
		}
	},

	"info" : {

		"about" : {
			title : "About",
			author : "Kion",
			created_on : "Jan 23, 2020",
			last_edit : "Jan 23, 2020"
		},

		"mascot" : {
			title : "About",
			author : "Kion",
			created_on : "Jan 23, 2020",
			last_edit : "Jan 23, 2020"
		},

		"resource" : {
			title : "About",
			author : "Kion",
			created_on : "Jan 23, 2020",
			last_edit : "Jan 23, 2020"
		}

	}

};
